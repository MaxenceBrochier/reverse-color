import os
from os.path import basename

def recupNom(nomwithext:str, tprt:bool):
    file = basename(nomwithext)
    extension = (os.path.splitext(file)[1])
    filename = (os.path.splitext(file)[0])
    if tprt == 1:
        return extension
    elif tprt == 0:
        return filename


def reverse(file:str):
    newname = recupNom(file, 0)
    ext = recupNom(file, 1)
    newname = newname + "REVERSE" + ext
    with open(file, "r") as f:
        with open(newname, "a") as revfile:
            for ligne in f:
                if len(ligne) >= 20:
                    for i in ligne:
                        if i == '0':
                            revfile.write('1')
                        elif i == '1':
                            revfile.write('0')
                else:
                    revfile.write(ligne)
                revfile.write("\n")

def reversePGM(file:str):
    newname = recupNom(file, 0)
    ext = recupNom(file, 1)
    newname = newname + "REVERSE" + ext
    with open(file, "r") as f:
        with open(newname, "a") as revfile:
            for ligne in f:
                if len(ligne) >= 20:
                    s = ligne.strip("\n\r")
                    l = s.split(" ")
                    for i in range(len(l)):
                        print(l)
                        if l[i] == '0':
                            revfile.write('50 ')
                        elif l[i] == '15':
                            revfile.write('20 ')
                        elif l[i] == '20':
                            revfile.write('15 ')
                        elif l[i] == '50':
                            revfile.write('0 ')
                elif len(ligne) < 20:
                    revfile.write(ligne)
                revfile.write("\n")